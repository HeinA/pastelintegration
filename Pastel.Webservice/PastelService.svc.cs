﻿using Pastel.Interface;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Pastel.Webservice
{
  public class PastelService : IPastelService
  {
    #region Private Utilities

    PasSDK.PastelPartnerSDK GetSDK(string connectionName)
    {
      PasSDK.PastelPartnerSDK sdk = new PasSDK.PastelPartnerSDKClass();
      sdk.SetLicense(ConfigurationManager.AppSettings["PastelLicencee"], ConfigurationManager.AppSettings["PastelAuthCode"]);
      sdk.SetDataPath(ConfigurationManager.ConnectionStrings[connectionName].ConnectionString);
      return sdk;
    }

    void ValidateResult(string result)
    {
      string[] results = result.Split('|');
      if (results.Length == 0) throw new PastelSDKException(-1, "An unknown error has occured.");

      int resultCode = 0;
      try
      {
        resultCode = int.Parse(results[0]);
      }
      catch
      {
        throw new PastelSDKException(-1, "An unknown error has occured while parsing the result number.");
      }

      switch (resultCode)
      {
        case 0: return;
        case 1: throw new PastelSDKException(resultCode, "File not found.");
        case 2: throw new PastelSDKException(resultCode, "Invalid number of fields.");
        case 3: throw new PastelSDKException(resultCode, "Record update not successful.");
        case 4: throw new PastelSDKException(resultCode, "Record insert not successful.");
        case 5: throw new PastelSDKException(resultCode, "Record does not exist in file.");
        case 6: throw new PastelSDKException(resultCode, "Data path does not exist.");
        case 7: throw new PastelSDKException(resultCode, "Access denied.");
        case 9: throw new PastelSDKException(resultCode, "End of file.");
        case 10: throw new PastelSDKException(resultCode, "Field number specified not valid.");
        case 11: throw new PastelSDKException(resultCode, "Invalid period number (1 to 13).");
        case 12: throw new PastelSDKException(resultCode, "Invalid date.");
        case 13: throw new PastelSDKException(resultCode, "Invalid account type (GDC).");
        case 14: throw new PastelSDKException(resultCode, "Invalid general ledger account number.");
        case 15: throw new PastelSDKException(resultCode, "General ledger account contains sub accounts.");
        case 16: throw new PastelSDKException(resultCode, "General ledger account number must be numeric.");
        case 17: throw new PastelSDKException(resultCode, "Invalid customer account code.");
        case 18: throw new PastelSDKException(resultCode, "Invalid supplier account code.");
        case 19: throw new PastelSDKException(resultCode, "Invalid inventory item code.");
        case 20: throw new PastelSDKException(resultCode, "Invalid salesman code.");
        case 21: throw new PastelSDKException(resultCode, "Invalid job code.");
        case 22: throw new PastelSDKException(resultCode, "Invalid Tax Type (0 to 30).");
        case 23: throw new PastelSDKException(resultCode, "Transaction amount cannot be less that the tax amount.");
        case 24: throw new PastelSDKException(resultCode, "Invalid open item transaction type - must be O (Original) or A (Allocation).");
        case 25: throw new PastelSDKException(resultCode, "There cannot be more than 500 lines in a batch.");
        case 26: throw new PastelSDKException(resultCode, "Invalid account description.");
        case 27: throw new PastelSDKException(resultCode, "Default group needs to set up in Pastel.");
        case 28: throw new PastelSDKException(resultCode, "Invalid document line type – must be 2, 5, or 7.");
        case 29: throw new PastelSDKException(resultCode, "Invalid exclusive / inclusive – must be 0 or 1.");
        case 30: throw new PastelSDKException(resultCode, "Invalid Entry Type (1 to 90).");
        case 31: throw new PastelSDKException(resultCode, "Duplicate inventory item.");
        case 32: throw new PastelSDKException(resultCode, "Invalid multi-store code.");
        case 33: throw new PastelSDKException(resultCode, "Invalid Currency Code.");
        case 99: throw new PastelSDKException(resultCode, "General Error (Normally due to long path names).");

        default: throw new PastelSDKException(resultCode, "An unknown error has occured.");
      }
    }

    bool IsEOF(string result)
    {
      string[] results = result.Split('|');
      if (results.Length == 0) throw new PastelSDKException(-1, "An unknown error has occured.");

      int resultCode = 0;
      try
      {
        resultCode = int.Parse(results[0]);
      }
      catch
      {
        throw new PastelSDKException(-1, "An unknown error has occured while parsing the result number.");
      }

      switch (resultCode)
      {
        case 0: return false;
        case 9: return true;

        default:
          ValidateResult(result);
          break;
      }

      throw new PastelSDKException(resultCode, "An unknown error has occured.");
    }

    #endregion

    Collection<TaxType> IPastelService.GetTaxTypes(string connectionName)
    {
      var sdk = GetSDK(connectionName);

      Collection<TaxType> taxTypes = new Collection<TaxType>();

      string s1 = sdk.GetRecord("ACCTDESC", 0, "");
      ValidateResult(s1);
      string[] descriptions = s1.Split('|');

      string s2 = sdk.GetRecord("ACCPRMTX", 0, "1");
      ValidateResult(s2);
      string[] rates = s2.Split('|');

      for (int i = 1; i < descriptions.Length; i++)
      {
        if (descriptions[i] != string.Empty)
        {
          string name = descriptions[i];
          decimal rate = decimal.Parse(rates[i - 27]) / 100;
          taxTypes.Add(new TaxType((short)(i - 32), name, rate));
        }
      }

      return taxTypes;
    }

    public string ImportDocument(string connectionName, DocumentType docType, bool isBatch, Document doc)
    {
      doc.PreValidate();

      var sdk = GetSDK(connectionName);
      sdk.OpenDocumentFiles();
      sdk.DefineDocumentHeader(doc.ToString());
      foreach (var l in doc.Lines)
      {
        sdk.DefineDocumentLine(l.ToString());
      }

      short docType1 = (short)docType;
      if (isBatch) docType1 += (short)100;

      string result = sdk.ImportDocument(docType1);
      sdk.CloseDocumentFiles();

      ValidateResult(result);

      return result.Split('|')[1];
    }
  }
}
