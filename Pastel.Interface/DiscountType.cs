﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pastel.Interface
{
  public enum DiscountType
  {
    None = 0,
    Settlement = 1,
    Overall = 2,
    Both = 3
  }
}
