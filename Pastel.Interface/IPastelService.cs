﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Pastel.Interface
{
  [ServiceContract(Namespace = Constants.WcfNamespace)]
  [ServiceKnownType(typeof(TaxType))]
  [ServiceKnownType(typeof(Document))]
  [ServiceKnownType(typeof(DocumentLine))]
  [ServiceKnownType(typeof(DocumentType))]
  [ServiceKnownType(typeof(DiscountType))]
  [ServiceKnownType(typeof(LineType))]
  public interface IPastelService
  {
    [OperationContract]
    Collection<TaxType> GetTaxTypes(string connectionName);

    [OperationContract]
    string ImportDocument(string connectionName, DocumentType docType, bool isBatch, Document doc);
  }
}
