﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Pastel.Interface
{
  [Serializable]
  public class PastelFieldException : Exception
  {
    protected PastelFieldException(SerializationInfo info, StreamingContext context)
      :base(info, context)
    {
    }

    public PastelFieldException(string message)
      : base(message)
    {
    }
  }
}
