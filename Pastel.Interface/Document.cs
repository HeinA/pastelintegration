﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Pastel.Interface
{
  [DataContract(IsReference = true, Namespace = Constants.WcfNamespace)]
  public class Document
  {
    public Document()
    {
      DocumentDate = DateTime.Now;
      ClosingDate = DateTime.Now;
      VatIncusive = false;
      ExchangeRate = 1;
      Message = string.Empty;
      DeliveryAddress = string.Empty;
      Address = string.Empty;
    }

    [DataMember]
    public string DocumentNumber { get; set; }

    [DataMember]
    public DateTime DocumentDate { get; set; }

    [DataMember]
    public string CustomerCode { get; set; }

    [DataMember]
    public string ReferenceNumber { get; set; }

    [DataMember]
    public DateTime ClosingDate { get; set; }

    [DataMember]
    public string Message { get; set; }

    [DataMember]
    public string DeliveryAddress { get; set; }

    [DataMember]
    public string Description { get; set; }

    [DataMember]
    public string TelephoneNumber { get; set; }

    [DataMember]
    public string FaxNumber { get; set; }

    [DataMember]
    public string Contact { get; set; }

    [DataMember]
    public bool VatIncusive { get; set; }

    [DataMember]
    public decimal DiscountPercentage { get; set; }

    [DataMember]
    public decimal ExchangeRate { get; set; }

    [DataMember]
    public string SalesAnalysisCode { get; set; }

    [DataMember]
    public short SettlementTermsCode { get; set; }

    [DataMember]
    public string JobCode { get; set; }

    [DataMember]
    public string ExemptReference { get; set; }

    [DataMember]
    public string Address { get; set; }

    [DataMember]
    public string ShipDeliverText { get; set; }

    [DataMember]
    public string FreightText { get; set; }

    [DataMember]
    public bool Deleted { get; set; }

    [DataMember]
    public bool Printed { get; set; }

    [DataMember]
    public bool OnHold { get; set; }

    [DataMember]
    Collection<DocumentLine> mLines;
    public Collection<DocumentLine> Lines
    {
      get { return mLines ?? (mLines = new Collection<DocumentLine>()); }
    }

    public void PreValidate()
    {
      Utilities.ValidateStringLength("DocumentNumber", DocumentNumber, 8);
      Utilities.ValidateStringLength("CustomerCode", CustomerCode, 7);
      Utilities.ValidateStringLength("ReferenceNumber", ReferenceNumber, 15);
      Utilities.ValidateStringLength("SalesAnalysisCode", SalesAnalysisCode, 5);
      Utilities.ValidateStringLength("JobCode", JobCode, 5);
      Utilities.ValidateStringLength("TelephoneNumber", TelephoneNumber, 16);
      Utilities.ValidateStringLength("FaxNumber", FaxNumber, 16);
      Utilities.ValidateStringLength("Contact", Contact, 16);
      Utilities.ValidateStringLength("Description", Description, 40);
      Utilities.ValidateStringLength("ExemptReference", ExemptReference, 16);
      Utilities.ValidateStringLength("ShipDeliverText", ShipDeliverText, 16);
      Utilities.ValidateStringLength("FreightText", FreightText, 10);

      Utilities.ValidateMultiStringLengths("Message", Message, 3, 30);
      Utilities.ValidateMultiStringLengths("DeliveryAddress", DeliveryAddress, 5, 30);
      Utilities.ValidateMultiStringLengths("Address", Address, 5, 30);

      if (SettlementTermsCode < 0 || SettlementTermsCode > 32) throw new PastelFieldException("SettlementTermsCode must be between 0 and 32");

      foreach (var l in Lines)
      {
        l.PreValidate();
      }
    }

    public override string ToString()
    {
      string[] messageLines = Message.Replace("\r\n", "\n").Split('\n');
      string[] daLines = DeliveryAddress.Replace("\r\n", "\n").Split('\n');
      string[] aLines = Address.Replace("\r\n", "\n").Split('\n');

      return string.Format("{0}|{1}|{2}|{3}|{4:dd/MM/yyyy}|{5}|{6}|{7:0000}|{8}|{9}|{10}|{11}|{12}|{13}|{14}|{15}|{16}|{17:00}|{18}|{19:dd/MM/yyyy}|{20}|{21}|{22}|{23}|{24}|{25}|{26}|{27}|{28}|{29}|{30}|{31}|{32}|{33}"
        , DocumentNumber
        , Deleted ? "Y" : " "
        , Printed ? "Y" : " "
        , CustomerCode
        , DocumentDate
        , ReferenceNumber
        , VatIncusive ? "Y" : "N"
        , DiscountPercentage * 100
        , messageLines.Length > 0 ? messageLines[0] : null
        , messageLines.Length > 1 ? messageLines[1] : null
        , messageLines.Length > 2 ? messageLines[2] : null
        , daLines.Length > 0 ? daLines[0] : null
        , daLines.Length > 1 ? daLines[1] : null
        , daLines.Length > 2 ? daLines[2] : null
        , daLines.Length > 3 ? daLines[3] : null
        , daLines.Length > 4 ? daLines[4] : null
        , SalesAnalysisCode
        , SettlementTermsCode
        , JobCode
        , ClosingDate
        , TelephoneNumber
        , Contact
        , FaxNumber
        , ExchangeRate
        , Description
        , ExemptReference
        , aLines.Length > 0 ? aLines[0] : null
        , aLines.Length > 1 ? aLines[1] : null
        , aLines.Length > 2 ? aLines[2] : null
        , aLines.Length > 3 ? aLines[3] : null
        , aLines.Length > 4 ? aLines[4] : null
        , ShipDeliverText
        , FreightText
        , OnHold ? "Y" : "N");
    }
  }
}
