﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Pastel.Interface
{
  [Serializable]
  public class PastelSDKException : Exception
  {
    protected PastelSDKException(SerializationInfo info, StreamingContext context)
      :base(info, context)
    {
    }

    public PastelSDKException(int errorCode, string message)
      : base(message)
    {
      ErrorCode = errorCode;
    }

    public int ErrorCode { get; private set; }
  }
}
