﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pastel.Interface
{
  static class Utilities
  {
    public static void ValidateStringLength(string fieldName, string fieldValue, int length)
    {
      if (fieldValue != null && fieldValue.Length > length) throw new PastelFieldException(string.Format("Property {0} may not be longer than {1}.", fieldName, length));
    }

    public static void ValidateMultiStringLengths(string fieldName, string fieldValue, int lines, int length)
    {
      string[] textLines = fieldValue.Replace("\r\n", "\n").Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
      if (textLines.Length > lines) throw new PastelFieldException(string.Format("Property {0} may not be contain more than {1} lines.", fieldName, lines));
      for (int i = 0; i < textLines.Length; i++)
      {
        ValidateStringLength(string.Format("{0} Line {1}", fieldName, i + 1), textLines[i], length);
      }
    }
  }
}
