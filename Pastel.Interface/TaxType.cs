﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Pastel.Interface
{
  [DataContract(IsReference = true, Namespace = Constants.WcfNamespace)]
  public class TaxType
  {
    public TaxType(short id, string name, decimal rate)
    {
      Id = id;
      Name = name;
      Rate = rate;
    }

    [DataMember]
    public short Id { get; set; }

    [DataMember]
    public string Name { get; set; }

    [DataMember]
    public decimal Rate { get; set; }
  }
}
