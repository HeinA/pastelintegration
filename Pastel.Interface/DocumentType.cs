﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pastel.Interface
{
  public enum DocumentType
  {
    Quotation = 1,
    SalesOrder = 2,
    CustomerInvoice = 3,
    CreditNote = 4,
    DebitNote = 5,
    PurchaseOrder = 6,
    GRN = 7,
    SupplierInvoice = 8,
    Return = 9,
    SupplierCredit = 10
  }
}
