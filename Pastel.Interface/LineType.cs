﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pastel.Interface
{
  public enum LineType
  {
    Invalid = 0,
    Inventory = 4,
    GL = 6,
    Remark = 7
  }
}
