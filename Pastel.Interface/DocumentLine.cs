﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Pastel.Interface
{
  [DataContract(IsReference = true, Namespace = Constants.WcfNamespace)]
  public class DocumentLine
  {
    public DocumentLine()
    {
    }

    [DataMember]
    public string Code { get; set; }

    [DataMember]
    public decimal Quantity { get; set; }

    [DataMember]
    public string Unit { get; set; }

    [DataMember]
    public decimal? ExclusiveUnitSellingPrice { get; set; }

    [DataMember]
    public decimal? InclusiveUnitSellingPrice { get; set; }

    [DataMember]
    public decimal? CostPrice { get; set; }

    [DataMember]
    public short TaxType { get; set; }

    [DataMember]
    public DiscountType DiscountType { get; set; }

    [DataMember]
    public decimal DiscountPercentage { get; set; }

    [DataMember]
    public string Description { get; set; }

    [DataMember]
    public LineType LineType { get; set; }

    [DataMember]
    public string MultiStore { get; set; }

    [DataMember]
    public string CostCode { get; set; }

    public void PreValidate()
    {
      Utilities.ValidateStringLength("Unit", Unit, 4);
      Utilities.ValidateStringLength("Code", Code, 15);
      Utilities.ValidateStringLength("Description", Description, 40);
      Utilities.ValidateStringLength("MultiStore", MultiStore, 3);
      Utilities.ValidateStringLength("CostCode", CostCode, 5);

      if (TaxType < 0 || TaxType > 30) throw new PastelFieldException("TaxType must be between 0 and 30");
      if (LineType == Interface.LineType.Invalid) throw new PastelFieldException("LineType is invalid.");
    }

    public override string ToString()
    {
      return string.Format("{0}|{1}|{2}|{3}|{4}|{5:00}|{6}|{7:0000}|{8}|{9}|{10}|{11}|{12}"
      , CostPrice
      , Quantity
      , ExclusiveUnitSellingPrice
      , InclusiveUnitSellingPrice
      , Unit
      , TaxType
      , (int)DiscountType
      , DiscountPercentage * 100
      , Code
      , Description
      , (int)LineType
      , MultiStore
      , CostCode);
    }
  }
}
