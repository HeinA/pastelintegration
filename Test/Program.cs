﻿using Pastel.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
  class Program
  {
    static void Main(string[] args)
    {
      Document doc = new Document();

      doc.CustomerCode = "XXX001";
      doc.ReferenceNumber = "Waybill No";
      doc.DeliveryAddress = "Somewhere\nOutju\nNamibia";
      doc.OnHold = true;

      DocumentLine line = new DocumentLine();
      line.Quantity = 1;
      line.ExclusiveUnitSellingPrice = 1317.45m;
      line.TaxType = 1;
      line.Code = "TT01";
      line.Description = "Line Description";
      line.LineType = LineType.Inventory;

      doc.Lines.Add(line);

      using (var svc = ServiceFactory.GetService<IPastelService>())
      {
        //var list = svc.Instance.GetTaxTypes("Namibia");

        var docNo = svc.Instance.ImportDocument("Namibia", DocumentType.CustomerInvoice, true, doc);
      }
    }
  }
}
